﻿using UnityEngine;
using System.Collections;

public class MouseAimCamera : MonoBehaviour
{
	public static MouseAimCamera instance;
	public float maxCameraRadius = 12.0F;
	public GameObject target;
	public float rotateSpeed = 5;
	public bool invert;

	private Vector3 offset;
	[SerializeField]
	private float vertical;
	[SerializeField]
	private float distance;
	private float minDistance = 4f;

	void Start ()
	{
		offset = new Vector3 (0, 0, maxCameraRadius);

		distance = maxCameraRadius;
	}

	void Update ()
	{
		int layerMask = ~((1 << 2) | (1 << 8));
		Vector3 fwd = transform.TransformDirection (Vector3.forward);
		RaycastHit hit;
		if (Physics.Raycast (target.transform.position, (transform.position - target.transform.position).normalized, out hit, maxCameraRadius, layerMask, QueryTriggerInteraction.Ignore) && Physics.Raycast (transform.position, fwd, maxCameraRadius, layerMask) && target.GetComponent<CharController> ().IsMidJump ()) {
			Debug.DrawRay (target.transform.position, transform.position - target.transform.position, Color.green);
			//print ("hit :" + hit.distance);
			if (hit.distance > 0.5f)
				distance = Mathf.Clamp (hit.distance - 1, minDistance, maxCameraRadius);
		} else
			distance = maxCameraRadius;
		Debug.DrawRay (transform.position, fwd * distance, Color.white);
	}

	void LateUpdate ()
	{		
		if (Time.timeScale != 0) {
			if (target == null)
				target = GameObject.FindGameObjectWithTag ("Player");
			if (maxCameraRadius <= 2)
				distance = 0;
		
			float direction = invert ? 1 : -1;
			offset = new Vector3 (0, 0, distance);
		
			float horizontal = Input.GetAxis ("Mouse X") * rotateSpeed;
			vertical += Input.GetAxis ("Mouse Y") * rotateSpeed * direction;
			if (Mathf.Abs (vertical) > 89)
				vertical = 89 * Mathf.Abs (vertical) / vertical;
			else if (maxCameraRadius != 0 && vertical < -60)
				vertical = -60;
			float desiredAngleX = target.transform.eulerAngles.y;
			target.transform.Rotate (0, horizontal, 0);
			Quaternion rotation = Quaternion.Euler (vertical, desiredAngleX, 0);
			transform.position = target.transform.position - (rotation * offset);
			if (distance != 0) {	
				transform.LookAt (target.transform);
			} else {
				transform.rotation = Quaternion.Euler (vertical, desiredAngleX, 0);
			}
//		if (Mathf.Abs (vertical) > 89)
//			vertical = 89 * Mathf.Abs (vertical) / vertical;
		}
	}

	public float ReturnDistance ()
	{
		return distance;
	}
}
