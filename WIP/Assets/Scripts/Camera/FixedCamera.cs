﻿using UnityEngine;
using System.Collections;

public class FixedCamera : MonoBehaviour
{
	public GameObject target;

	void LateUpdate () {
		transform.LookAt (target.transform);
	}

}
