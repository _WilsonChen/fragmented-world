﻿using UnityEngine;
using System.Collections;

public class CameraAlignScript : MonoBehaviour {
	private Camera defaultCamera;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		defaultCamera = Camera.main;
		transform.LookAt (transform.position + defaultCamera.transform.rotation * Vector3.forward, defaultCamera.transform.rotation * Vector3.up);
	}
}
