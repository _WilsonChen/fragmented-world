﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScenePopulator : MonoBehaviour {
	public GameObject[] objectsToSpawn;

	public int amountToSpawn;
	public float excessHeight;

	[SerializeField]
	private List<GameObject> clutterList;
	private GameObject currentPlayer;
	float radius;
	int counter;
	Vector3 minPoint;
	Vector3 maxPoint;
	// Use this for initialization
	void Start () {
		if (excessHeight < 0)
			excessHeight = 0;
		Object[] tempList = Resources.FindObjectsOfTypeAll (typeof(GameObject));
		currentPlayer = GameObject.FindGameObjectWithTag ("Player");
		List<Object> realList = new List<Object> ();
		GameObject temp;

		foreach (GameObject obj in tempList) {
			if (obj is GameObject) {
				temp = (GameObject)obj;
				if (temp.hideFlags == HideFlags.None)
					realList.Add ((GameObject)obj);
			}
		}
		List<GameObject> platformArray = new List<GameObject> ();
		foreach (GameObject obj in realList) {
			if (obj.layer == 9)
				platformArray.Add ((GameObject)obj);
		}
		foreach (GameObject obj in platformArray) {
			SetMinValue (obj.transform.position);
			SetMaxValue (obj.transform.position);
		}


		while (counter < amountToSpawn) {
			
			GameObject objectToSpawn = objectsToSpawn [Random.Range (0, objectsToSpawn.Length)];
			Vector3 objectExtents = objectToSpawn.GetComponent<Collider> ().bounds.extents;
			radius = Mathf.Sqrt (objectExtents.x * objectExtents.x + objectExtents.z * objectExtents.z);
			Vector3 tempMinPoint =  minPoint - new Vector3 (Camera.main.farClipPlane, excessHeight, Camera.main.farClipPlane);
			Vector3 tempMaxPoint = maxPoint + new Vector3 (Camera.main.farClipPlane, excessHeight, Camera.main.farClipPlane);
			Vector3 chosenPos = new Vector3 (Random.Range (tempMinPoint.x, tempMaxPoint.x), tempMaxPoint.y + radius * 2, Random.Range (tempMinPoint.z, tempMaxPoint.z));
			RaycastHit info;
			if (!Physics.SphereCast (chosenPos, radius, Vector3.down, out info)) {
				chosenPos.y = Random.Range (tempMinPoint.y, tempMaxPoint.y);
				GameObject obj = (GameObject)Instantiate (objectToSpawn, chosenPos, Quaternion.identity);
				obj.transform.SetParent (transform);
				clutterList.Add (obj);
				counter++;
			} else
				print ("Hit");
		}
	}
	private void SetMinValue(Vector3 value)
	{
		SetMinRef (ref minPoint.x, value.x);
		SetMinRef (ref minPoint.y, value.y);
		SetMinRef (ref minPoint.z, value.z);

	}

	private void SetMinRef(ref float reference, float value)
	{
		if (value < reference)
			reference = value;
	}

	private void SetMaxValue(Vector3 value)
	{
		SetMaxRef (ref maxPoint.x, value.x);
		SetMaxRef (ref maxPoint.y, value.y);
		SetMaxRef (ref maxPoint.z, value.z);

	}

	private void SetMaxRef(ref float reference, float value)
	{
		if (value > reference)
			reference = value;
	}
	// Update is called once per frame
	void Update () {
		

	}
}
