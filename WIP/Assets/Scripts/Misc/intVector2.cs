﻿using System;

public struct intVector2
{
	public int x;
	public int y;

	public intVector2 (int x, int y)
	{
		this.x = x;
		this.y = y;
	}
	public override bool Equals (object obj)
	{
		if (obj.GetType () != this.GetType ())
			return false;
		intVector2 other = (intVector2)obj;

		return this == other;
	}

	public override int GetHashCode ()
	{
		return x ^ y;
	}
	public static bool operator == (intVector2 iV1, intVector2 iV2)
	{
		return iV1.x == iV2.x && iV1.y == iV2.y;
	}

	public static bool operator != (intVector2 iV1, intVector2 iV2)
	{
		return !(iV1.x == iV2.x && iV1.y == iV2.y);
	}
}


