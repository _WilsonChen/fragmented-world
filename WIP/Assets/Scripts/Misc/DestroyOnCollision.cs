﻿using UnityEngine;
using System.Collections;

public class DestroyOnCollision : MonoBehaviour {
	void Start()
	{
		transform.Rotate (Vector3.up, Random.Range (0f, 360f));
		Renderer[] list = GetComponentsInChildren<Renderer> ();
		if (list != null) {
			foreach (Renderer rd in list)
				rd.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		}
	}
	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer != 2)
			Destroy (gameObject);
	}
}
