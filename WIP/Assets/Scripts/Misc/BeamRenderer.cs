﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeamRenderer : MonoBehaviour {
	public bool noFragmentBeam;
	public Material material;
	private LineRenderer lr;
	// Use this for initialization
	void Start () {
		lr = GetComponent<LineRenderer> ();
		List<ScatterBlock> blockList;
		if (GetComponent<ReleaseScatterBlockScript> ()) {
			blockList = GetComponent<ReleaseScatterBlockScript> ().ReturnList ();
			if (blockList != null) {
				foreach (ScatterBlock block in blockList) {
					GameObject obj = (GameObject)Instantiate (new GameObject ("Line"), block.transform.position, Quaternion.identity);
					obj.transform.parent = transform;
					LineRenderer line = obj.AddComponent<LineRenderer> ();
					line.material = material;
					Vector3[] templist = new Vector3[] {
						obj.transform.position,
						obj.transform.position + Vector3.down * Camera.main.farClipPlane
					};
					line.SetPositions (templist);
				}
			}
		}
		if (!lr) {
			lr = gameObject.AddComponent<LineRenderer> ();
			lr.material = material;
		}
		Vector3[] posList = new Vector3[] { transform.position, transform.position + Vector3.up * Camera.main.farClipPlane };
		lr.SetPositions (posList);
		if (noFragmentBeam)
			lr.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
