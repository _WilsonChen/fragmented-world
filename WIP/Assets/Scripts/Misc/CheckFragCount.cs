﻿using UnityEngine;
using System.Collections;

public class CheckFragCount : MonoBehaviour {
	public GameObject[] objList;

	private FragmentTracker tracker;
	// Use this for initialization
	void Start () {
		foreach (GameObject obj in objList)
			obj.SetActive (false);
		tracker = GameObject.FindGameObjectWithTag ("Player").GetComponent<FragmentTracker> ();
		if (tracker) {
			if (tracker.ReturnFragCount() > 0)
			for (int i = 0; i < tracker.ReturnFragCount (); i++)
				objList [i].SetActive (true);
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
