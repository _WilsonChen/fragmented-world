﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingBeamGenerator : MonoBehaviour {
	public Material material;

	private LineRenderer lr;
	private List<ScatterBlock> blocks;

	// Use this for initialization
	void Start () {
		lr = GetComponent<LineRenderer> ();
		blocks = new List<ScatterBlock>();
		blocks.AddRange (GetComponentsInChildren<ScatterBlock> ());
			
		if (!lr) {
			lr = gameObject.AddComponent<LineRenderer> ();
			lr.material = material;
			List<Vector3> blockList = new List<Vector3>(GetComponent<MovingBlock> ().ReturnList ());
			if (blockList != null) {
				lr.SetVertexCount( blockList.Count);
				lr.SetPositions (blockList.ToArray());
			}
		}
	}

	// Update is called once per frame
	void Update () {
		if (blocks.Count > 0) {
			bool isFinished = true;
			foreach (ScatterBlock currentblock in blocks) {
				if (!currentblock.IsFinished ()) {
					isFinished = false;
					break;
				}
			}
			lr.enabled = isFinished;
		}
	}
}
