﻿using UnityEngine;
using System.Collections;

public static class InitiateTransition {
	public static void Fade (string scene, Color col, float damp){
		if (!GameObject.Find ("Fader")) {
			GameObject init = new GameObject ();
			init.name = "Fader";
			init.AddComponent<LostWoodsExitBehaviour> ();
			LostWoodsExitBehaviour scr = init.GetComponent<LostWoodsExitBehaviour> ();
			scr.fadeDamp = damp;
			scr.fadeScene = scene;
			scr.fadeColor = col;
			scr.start = true;
		}
	}
}
