﻿	using UnityEngine;
using System.Collections;

public class LostWoodsExitBehaviour : MonoBehaviour {

	public bool start = false;
	public float fadeDamp = 0.0f;
	public string fadeScene;
	public float alpha = 0.0f;
	public Color fadeColor;
	public bool isFadedIn;
	public bool playerTransition;

	// Use this for initialization
	void Start () {
	}

	void OnGUI(){
		if (!start)
			return;
		GUI.color = new Color (GUI.color.r, GUI.color.g, GUI.color.b, alpha);

		Texture2D myTex;
		myTex = new Texture2D (1, 1);
		myTex.SetPixel (0, 0, fadeColor);
		myTex.Apply ();

		GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), myTex);

		if (isFadedIn) {
			alpha = Mathf.Lerp (alpha, -0.1f, fadeDamp * Time.deltaTime);
		} else {
			alpha = Mathf.Lerp (alpha, 1.1f, fadeDamp * Time.deltaTime);
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (alpha >= 1 && !isFadedIn) {
			readyToTransition ();
			DontDestroyOnLoad (gameObject);
		} else if (alpha <= 0 && isFadedIn) {
			playerTransition = false;
			Destroy (gameObject);
		}
	}

	void readyToTransition()
	{
		playerTransition = true;
		isFadedIn = true;
	}
}
