﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class SpawnTreeTiles : MonoBehaviour {
	public GameObject tile;
	// Use this for initialization
	[SerializeField]
	private List<Vector3> posList;
	void Start () {
		posList = new List<Vector3> ();
		Bounds bound = gameObject.GetComponent<Renderer> ().bounds;

		int radii = 2;
		for (int x = -radii; x <= radii; x++)
		{
			for (int y = -radii; y <= radii; y++)
			{
				double dx = x * bound.size.x;
				double dy = y * bound.size.z;
				double distanceSquared = dx * dx + dy * dy;
				double radiusSquared = (bound.size.x * bound.size.x + bound.size.z * bound.size.z) * radii;
				if (distanceSquared <= radiusSquared)
				{
					Vector3 tempPos = new Vector3 ((float)dx + transform.position.x, transform.position.y, (float)dy + transform.position.z);
					Vector3 castPos = tempPos;
					castPos.y -= 10;
					int layerMask = 1 << 9;
					Ray castedRay = new Ray (castPos, Vector3.up);
					if (!Physics.Raycast(castedRay, 20, layerMask))
						posList.Add(tempPos);
				}
			}
		}
		foreach (Vector3 pos in posList) {
			GameObject obj = (GameObject)Instantiate (tile, pos, Quaternion.identity);
			obj.transform.SetParent (transform);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDrawGizmos () {
		foreach (Vector3 pos in posList) {
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere (pos, 2);

			Gizmos.color = Color.yellow;
			Gizmos.DrawLine (pos, pos + Vector3.up * 20);
		}
	}
}
