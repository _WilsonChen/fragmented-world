﻿using UnityEngine;
using System.Collections;

public class TreePopulator : MonoBehaviour {
	public int amount;
	public GameObject[] trees; 

	Vector3 minPoint;
	Vector3 maxPoint;
	float maxDistance;
	Vector3 debugPoint;

	// Use this for initialization
	void Start () {
		int counter = 0;
		Renderer r = gameObject.GetComponent<Renderer> ();
		minPoint = new Vector3(r.bounds.min.x, r.bounds.max.y, r.bounds.min.z);
		maxPoint = r.bounds.max;
		foreach(GameObject g in trees) {
			Bounds bound = g.GetComponent<Renderer> ().bounds;
			float distance = bound.max.y - bound.min.y;
			if (maxDistance < distance)
				maxDistance = distance;
		}
		int limit = 0;
		while (counter < amount) {
			if (limit > 10)
				break;
			print ("attempt to spawn");
			GameObject objectToSpawn = trees [Random.Range (0, trees.Length - 1)];
			Bounds treeBounds = objectToSpawn.GetComponent<Renderer> ().bounds;
			Vector3 extents = treeBounds.extents;
			Vector3 chosenPos = new Vector3 (Random.Range (minPoint.x, maxPoint.x), maxPoint.y, Random.Range (minPoint.z, maxPoint.z));
			float radius = Mathf.Sqrt (extents.x * extents.x + extents.z * extents.z);
			Vector3 castpos = new Vector3 (chosenPos.x, chosenPos.y + maxDistance + radius * 2, chosenPos.z);
			debugPoint = castpos;
			Ray castedRay = new Ray (castpos, Vector3.down);
			int mask = 1 << 10;
			if (!Physics.SphereCast (castedRay, radius, maxDistance, mask)) {
				chosenPos.y = Random.Range (minPoint.y, maxPoint.y);
				GameObject obj = (GameObject)Instantiate (objectToSpawn, chosenPos, Quaternion.identity);
				obj.transform.SetParent (transform);
				counter++;
			} else
				limit++;
		}
	}

	void Update() {
	}
	 //Update is called once per frame
//	void OnDrawGizmos () {
//		Gizmos.color = Color.blue;
//		Gizmos.DrawSphere (minPoint, 2);
//
//		Gizmos.color = Color.red;
//		Gizmos.DrawSphere (maxPoint, 2);
//
//		Gizmos.color = Color.yellow;
//		Gizmos.DrawSphere (debugPoint, 2);
//	}
}
