﻿using UnityEngine;
using System.Collections;

public class CollaspeBranch : MonoBehaviour {
	public FallBlock[] branches;

	FallBlock root;
	ScriptStorage storage;
	// Use this for initialization
	void Awake () {
		root = (FallBlock)gameObject.AddComponent<FallBlock> ();
		storage = (ScriptStorage)gameObject.AddComponent<ScriptStorage> ();
		storage.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		bool collapseRoot = true;
		foreach (FallBlock block in branches) {
			if (block.IsFinished ())
				collapseRoot = false;
		}
		if (collapseRoot) {
			storage.enabled = true;
			root.TriggerPlatform ();
		} else
			storage.enabled = false;
	}
}
