﻿using UnityEngine;
using System.Collections;

public class CondenseFog : MonoBehaviour {
	private bool begin;
	private float counter;

	// Use this for initialization
	void Start () {
		begin = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (begin) {
			counter += 5 * Time.deltaTime;
			if (RenderSettings.fogEndDistance > 80)
				RenderSettings.fogEndDistance = RenderSettings.fogEndDistance - counter;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player"))
			begin = true;
	}
}
