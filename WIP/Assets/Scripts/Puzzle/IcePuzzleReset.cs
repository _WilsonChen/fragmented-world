﻿using UnityEngine;
using System.Collections;

public class IcePuzzleReset : MonoBehaviour {
	public FragmentReleaseGroup assignedGroup;

	void OnTriggerEnter(Collider other)
	{
		assignedGroup.ResetPuzzle ();
	}
}
