﻿using UnityEngine;
using System.Collections;

public class ActivateScripts : MonoBehaviour {
	public MonoBehaviour[] scripts;
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			foreach (MonoBehaviour script in scripts) {
				foreach (Collider coll in script.gameObject.GetComponents<Collider>())
					coll.enabled = true;
				script.enabled = true;
			}
		}
	}
}
