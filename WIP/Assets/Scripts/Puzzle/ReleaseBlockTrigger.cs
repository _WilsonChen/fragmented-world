﻿using UnityEngine;
using System.Collections;

public class ReleaseBlockTrigger : MonoBehaviour {
	
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("Player"))
			gameObject.SetActive (false);
	}

	public void ResetBlock()
	{
		gameObject.SetActive (true);
	}

}
