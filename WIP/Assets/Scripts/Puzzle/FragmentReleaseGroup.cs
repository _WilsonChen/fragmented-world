﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class FragmentReleaseGroup : MonoBehaviour {
	public GameObject platform;
	public ReleaseScatterBlockScript fragment;
	[SerializeField]
	private List<ReleaseBlockTrigger> blockList;
	// Use this for initialization
	void Awake () {
		blockList = new List<ReleaseBlockTrigger> ();
		blockList.AddRange (gameObject.GetComponentsInChildren<ReleaseBlockTrigger> ());
	}

	// Update is called once per frame
	void Update () {
		bool releaseBlock = true;
		if (fragment != null) {
			foreach (ReleaseBlockTrigger block in blockList) {
				if (block.gameObject.activeInHierarchy)
					releaseBlock = false;
			}
			if (releaseBlock) {
				fragment.DoTask ();
				platform.SetActive (false);
			}
		}
	}

	public void ResetPuzzle()
	{
		foreach (ReleaseBlockTrigger block in blockList) {
			block.ResetBlock ();
		}
	}
}
