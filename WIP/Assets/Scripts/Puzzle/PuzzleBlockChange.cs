﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PuzzleBlockChange : MonoBehaviour {
	public int correctIndex;
	public int initialIndex;

	private List<Material> materialList;
	private MeshRenderer currentMat;
	private int currentIndex;
	private bool isActive;

	void Awake()
	{
		currentMat = GetComponent<MeshRenderer> ();
		isActive = true;
	}
		
	private void SetMaterial(int pointer)
	{
		if (materialList != null)
			currentMat.material = materialList [pointer];
	}
	//Method to change the colour so that it can be called from the player class
	public void ChangeColour(){
		if (isActive) {
			currentIndex = currentIndex >= materialList.Count - 1 ? 0 : currentIndex + 1;
			SetMaterial (currentIndex);
		}
	}

	public void setMaterialList(List<Material> materialList)
	{
		this.materialList = materialList;
		int count = materialList.Count - 1;
		if (correctIndex < count)
			correctIndex = count;
		currentIndex = initialIndex;
		SetMaterial (initialIndex);
	}

	public bool IsCorrectIndex()
	{
		return currentIndex == correctIndex;
	}

	public void DisableBlocks()
	{
		isActive = false;
	}
}
