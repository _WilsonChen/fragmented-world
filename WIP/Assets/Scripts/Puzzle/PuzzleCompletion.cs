﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PuzzleCompletion : MonoBehaviour {
	public List<Material> materialList;
	public GameObject spawnedObject;
	private bool solved;
	private List<PuzzleBlockChange> blockList;

	// Use this for initialization
	void Start () {
		blockList = new List<PuzzleBlockChange> ();
		foreach (PuzzleBlockChange block in gameObject.GetComponentsInChildren<PuzzleBlockChange>()) {
			blockList.Add (block);
			block.setMaterialList (materialList);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (!solved) {
			completed ();
		}
		else {
			foreach (PuzzleBlockChange block in blockList)
				block.DisableBlocks ();
			spawnedObject.SetActive (true);
		}
	}

	//Method to detect whether the puzzle is completed or not
	//Completed it determined on the puzzleSuceed status of each employee
	//I don't know what it is meant to do when it is completed so I have left it open-ended
	void completed(){
		solved = true;
		foreach (PuzzleBlockChange o in blockList)
		{
			if (!o.IsCorrectIndex()) {
				solved = false;	
			}
		}
	}
}
