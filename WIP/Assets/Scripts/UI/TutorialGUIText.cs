﻿using UnityEngine;
using System.Collections;
using System.Text;

public class TutorialGUIText : MonoBehaviour {
	public string moveText;
	public string lookText;
	public Texture up;
	public Texture down;
	public Texture left;
	public Texture right;
	public GUIStyle style;
	private bool show;
	public float width;
	public float height;

	private float iconWidth;
	private float iconHeight;
	// Use this for initialization
	void Start () {
		show = false;
		width = 600;
		height = 200;
		iconWidth = 90;
		iconHeight = 40;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnGUI () {
		if (show) {
			Rect baseRect = new Rect (Screen.width / 2 - width / 2, 3 * Screen.height / 4 - height / 2, width, height);
			GUI.BeginGroup (baseRect, style);
			GUI.DrawTexture (new Rect (iconWidth / 2 + iconWidth / 5, 0, iconWidth, iconHeight), up);
			GUI.DrawTexture (new Rect (iconWidth / 2 + iconWidth / 5, iconHeight * 2 + iconHeight / 5 * 2, iconWidth, iconHeight), down);
			GUI.DrawTexture (new Rect (0, iconHeight + iconHeight / 5, iconWidth, iconHeight), left);
			GUI.DrawTexture (new Rect (iconWidth + iconWidth / 5 * 2, iconHeight + iconHeight / 5, iconWidth, iconHeight), right);
			GUI.Box (new Rect (iconWidth / 2 + iconWidth / 5, iconHeight * 4, iconWidth, iconHeight), moveText, style);
			GUI.Box (new Rect (baseRect.size.x - (iconWidth / 2 + iconWidth / 5 + iconWidth), iconHeight * 4, iconWidth, iconHeight), lookText, style);
			GUI.EndGroup ();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player"))
			show = true;
	}

	void OnTriggerExit (Collider other)
	{
		if (other.CompareTag ("Player"))
			show = false;
	}
}
