﻿using UnityEngine;
using System.Collections;

public class UIRemoveScript : MonoBehaviour {
	public KeyCode setInput;
	public KeyCode setAltInput;

	public bool useMouse;
	private float counter;
	private bool mouseUsed;
	// Use this for initialization
	void Start () {
		counter = 0;
		mouseUsed = false;
	}
	
	// Update is called once per frame
	void Update () {
		if (useMouse) {
			if (useMouse && (Input.GetAxis ("Mouse X") != 0 || Input.GetAxis ("Mouse Y") != 0))
				mouseUsed = true;
		}
		else if (Input.GetKey (setInput) || Input.GetKey (setAltInput))
			gameObject.SetActive (false);
		if (mouseUsed)
			counter += Time.deltaTime;
		if (counter > 1)
			gameObject.SetActive (false);
	}
}
