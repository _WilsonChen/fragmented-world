﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StaticSlider : MonoBehaviour {
	public Slider[] sliderList;
	
	// Update is called once per frame
	public void UpdateValues(float value)
	{
		foreach (Slider slide in sliderList)
			slide.value = value;
	}
}
