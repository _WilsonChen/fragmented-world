﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class TutorialHideScript : MonoBehaviour {
	public int size;
	[SerializeField]
	private List<GameObject> elements;
	// Use this for initialization
	void Start () {
		elements = new List<GameObject> ();
		int i = 0;
		foreach (Transform obj in transform) {
			if (i >= size)
				break;
			if (!gameObject.Equals (obj))
				elements.Add (obj.gameObject);
			i++;
		}
	}
	
	// Update is called once per frame
	void Update () {
		bool hideSlide = true;
		foreach (GameObject obj in elements) {
			if (obj.activeSelf)
				hideSlide = false;
		}
		gameObject.SetActive (!hideSlide);
	}
}
