﻿using UnityEngine;
using System.Collections;
public struct SkyColourVar {
	public float fogDistance;
	public Color savedColor;

	public SkyColourVar(float num, Color chosenColor)
	{
		fogDistance = num;
		savedColor = chosenColor;
	}
}
public class SkyColourManager : MonoBehaviour {
	public Color[] colorsToCycle;
	public bool noFog;

	public Color sky;
	public Color equator;
	public Color ground;
	// Use this for initialization

	private SkyColourVar current;
	private SkyColourVar proposed;
	private SkyColourVar saved;
	private SkyColourVar switched;

	[SerializeField]
	private float counter;
	private float rate;
	private bool cycle;
	void OnEnable () {
		CharController.OnEnter += OnRestore;
		CharController.OnExit += OnFellOff;
	}
	void Start () {
		equator = RenderSettings.ambientEquatorColor;
		ground = RenderSettings.ambientGroundColor;
		sky = RenderSettings.ambientSkyColor;

		saved.fogDistance = RenderSettings.fogEndDistance;
		current.fogDistance = saved.fogDistance;
		proposed.fogDistance = saved.fogDistance;

		switched.fogDistance = 50;
		switched.savedColor = new Color (0, 0, 0, 0);
		rate = 0.1f;
		saved.savedColor = sky;
		proposed.savedColor = colorsToCycle [Random.Range (0, colorsToCycle.Length)];
		counter = 0;

		cycle = true;
	}
	
	// Update is called once per frame
	void Update () {
		counter +=  rate * Time.deltaTime;
		current.savedColor = Color.Lerp(saved.savedColor, proposed.savedColor, counter);
		current.fogDistance = Mathf.Lerp (saved.fogDistance, proposed.fogDistance, counter);
		if (counter > 1 && cycle) {
			saved.savedColor = current.savedColor;
			bool similar = true;
			while (similar) {
				Color tempColor = colorsToCycle [Random.Range (0, colorsToCycle.Length)];
				if (!tempColor.Equals (proposed.savedColor)) {
					proposed.savedColor = tempColor;
					similar = false;
				}
			}
			counter = 0;
			saved.fogDistance = current.fogDistance;
		}
		RenderSettings.ambientSkyColor = current.savedColor;
		Camera.main.backgroundColor = current.savedColor;
		RenderSettings.fogColor = current.savedColor;
		if (!noFog)
			RenderSettings.fogEndDistance = current.fogDistance;
		sky = current.savedColor;

	}

	void OnFellOff()
	{
		saved.savedColor = current.savedColor;
		saved.fogDistance = current.fogDistance;
		proposed.savedColor = switched.savedColor;
		proposed.fogDistance = switched.fogDistance;
		rate = 1;
		counter = 0;
		cycle = false;
	}

	void OnRestore()
	{
		proposed.fogDistance = saved.fogDistance;
		saved.savedColor = current.savedColor;
		proposed.fogDistance = Camera.main.farClipPlane;
		saved.fogDistance = switched.fogDistance;
		rate = 0.5f;
		counter = 0;
		cycle = true;
	}
}
