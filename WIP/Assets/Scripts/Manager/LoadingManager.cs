﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
using System.Collections;
using System.Collections.Generic;
using System;

public class LoadingManager : MonoBehaviour {
	public static LoadingManager instance;

	public GameObject player;
	int chosenScene;
	private int levelCount;
	private CursorLockMode wantedMode;
	private float counter;

	void Awake()
	{
		if (instance == null) {
			instance = this;
			DontDestroyOnLoad (gameObject);
			Cursor.lockState = CursorLockMode.Locked;
			wantedMode = CursorLockMode.Locked;
			counter = 0;
		} else {
			Destroy (gameObject);
		}
			
	}
		
	// Update is called once per frame
	void Update () {
		counter += Time.deltaTime;
		if (player == null)
			player = GameObject.FindGameObjectWithTag ("Player");
		try {
			if (!player.transform.parent.gameObject.activeSelf) {
			player.SetActive (true);
			player.transform.parent = null;
			}
		} catch {
		}
		char parseInt = ' ';
		if (Input.inputString.Length > 0)
			parseInt = Input.inputString [0];



		if(Input.GetKey(KeyCode.F1))
			LoadingScreenManager.LoadScene (7);

		if(Input.GetKey(KeyCode.F2))
			LoadingScreenManager.LoadScene (12);

		if(Input.GetKey(KeyCode.F3))
			LoadingScreenManager.LoadScene (16);
		

		}

		

	public void OnLevelComplete()
	{
		print (SceneManager.GetActiveScene().name);
		Analytics.CustomEvent ("LevelComplete", new Dictionary<string, object> {{ "Time for level Completion", counter}, {"Level name", SceneManager.GetActiveScene ().name}});
		counter = 0;
	}
		
}
