﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System;

public class LevelChange : MonoBehaviour {
	public int index;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
				LoadingScreenManager.LoadScene (index);
		}
		try {
		GameObject.Find ("Manager").GetComponent<LoadingManager> ().OnLevelComplete ();
		}
		catch {
			print ("No manager exists");
		}
	}
}
