﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour {
	[SerializeField]
	GameObject[] particleList;
	GameObject player;
	// Use this for initialization
	void Start () {
		particleList = GameObject.FindGameObjectsWithTag ("Particles");
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		foreach (GameObject obj in particleList) {
			ParticleSystem.EmissionModule system = obj.GetComponent<ParticleSystem> ().emission;
			system.enabled = Vector3.Distance (player.transform.position, obj.transform.position) < RenderSettings.fogEndDistance;
		}
	}
}
