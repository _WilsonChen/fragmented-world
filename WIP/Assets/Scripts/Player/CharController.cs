﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CharController : MonoBehaviour {
	public delegate void LeaveArea ();
	public static event LeaveArea OnExit;

	public delegate void EnterArea ();
	public static event EnterArea OnEnter;

    public float speed = 6.0F;
    public float jumpSpeed = 8.0F;
    public float gravity = 20.0F;
    public float neutralAirResist;
    public int damp;
    public int airDamp;
    public string scene;
    public float transitionTime;
    public float fallLimit;

    [SerializeField]
    private Vector3 moveDirection;
    private Vector2 axisRaw;
    private Vector3 safeBlockPosition;
    [SerializeField]
    private Vector3 neutralAirDirection;

    private bool ground;
    private bool taggedCeiling;
    private bool fellOff;
    private bool hasJumped;
	private bool triggeredEvent;
    private float terminalVelocity;
    private float fallSpeed;
    [SerializeField]
    private Vector2 skyboxRange;

    //Platform movement variables

	[SerializeField]
    private Transform activePlatform;
    private Vector3 activeLocalPlatformPoint;
    private Vector3 activeGlobalPlatformPoint;
    private Vector3 lastPlatformVelocity;

    private GameObject currentPuzzleBlock;

    private AudioSource[] sfx;
    private AudioSource jump, land;

    void Start()
    {
		triggeredEvent = false;
        sfx = GetComponents<AudioSource>();
        jump = sfx[0];
        land = sfx[1];

        moveDirection = Vector3.zero;
        axisRaw = Vector2.zero;
        safeBlockPosition = Vector3.zero;
        neutralAirDirection = Vector3.zero;

        ground = false;
        taggedCeiling = false;
        fellOff = false;
        hasJumped = false;

        terminalVelocity = gravity * 5;
        fallSpeed = 0;

        currentPuzzleBlock = new GameObject();

        if (fallLimit <= 0)
            fallLimit = 10f;
        FindBlockLimit();
    }
    private void FindBlockLimit()
    {
        skyboxRange = new Vector2(0, 0);
        foreach (GameObject current in GameObject.FindGameObjectsWithTag("SafepointBlock")) {
            float currentY = current.transform.position.y;
            if (skyboxRange.x > currentY)
                skyboxRange.Set(currentY, skyboxRange.y);
            else if (skyboxRange.y < currentY)
                skyboxRange.Set(skyboxRange.x, currentY);
        }
    }
    void Update() {
        CharacterController controller = GetComponent<CharacterController>();
        ground = controller.isGrounded;

        terminalVelocity = gravity * 5;
        float airResist = ground ? 0 : jumpSpeed - moveDirection.y;
        airResist = airResist > terminalVelocity ? terminalVelocity : airResist;

        decimal determinedDamp = ground ? (decimal)damp : (decimal)(airDamp * (1 - (airResist / terminalVelocity)));
        decimal currentDamp = determinedDamp != 0 ? (decimal)(1 / determinedDamp) : 0;
        if (fellOff)
            currentDamp = 0;
        axisRaw = new Vector2(SetDirection(axisRaw.x, "Horizontal", (float)currentDamp), SetDirection(axisRaw.y, "Vertical", (float)currentDamp));
        //axisRaw /= moveDirection.y / jumpSpeed;

        moveDirection = new Vector3(axisRaw.x * speed, moveDirection.y, axisRaw.y * speed);
        moveDirection = transform.TransformDirection(moveDirection);
        if ((controller.collisionFlags & CollisionFlags.Above) != 0 && !taggedCeiling) {
            moveDirection.y = 0;
            neutralAirDirection.y = moveDirection.y;
            taggedCeiling = true;
        }
        if (ground) {

            if (ground && fellOff) {
                land.Play();
            }

			triggeredEvent = false;
            taggedCeiling = false;
            fellOff = false;
            hasJumped = false;
            moveDirection.y = 0;

            if (Input.GetButtonDown("Jump")) {
                jump.Play();
                moveDirection.y = jumpSpeed;
                hasJumped = true;
            }
        } else if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0) {
            moveDirection = neutralAirDirection;

        } else {
            if (hasJumped)
                transform.parent = null;
        }
		if (transform.position.y < skyboxRange.x && !fellOff && !triggeredEvent) {
			if (OnExit != null) {
				OnExit ();
			}
			print ("Has exited");
			triggeredEvent = true;
		}
        if (transform.position.y > skyboxRange.x - fallLimit || fellOff)
            moveDirection.y -= gravity * Time.deltaTime;
        else {
            transform.position = new Vector3(safeBlockPosition.x, skyboxRange.y + fallLimit, safeBlockPosition.z);
            axisRaw = Vector2.zero;
            activePlatform = null;
            fellOff = true;
			triggeredEvent = false;
			if (OnEnter != null) {
				OnEnter();
			}
			print ("Has entered");
        }
        if (!ground)
            fallSpeed = moveDirection.y;
        PlatformMovement(controller);
        controller.Move((moveDirection) * Time.deltaTime);
        GetPlatform();
        neutralAirDirection = new Vector3(moveDirection.x - ReturnClampedValues(moveDirection.x, (float)neutralAirResist * ReturnSignage(moveDirection.x) * Time.deltaTime),
                                          moveDirection.y,
                                          moveDirection.z - ReturnClampedValues(moveDirection.z, (float)neutralAirResist * ReturnSignage(moveDirection.z) * Time.deltaTime));

    }

    void PlatformMovement(CharacterController controller)
    {
        if (activePlatform != null) {
            Vector3 newGlobalPlatformPoint = activePlatform.TransformPoint(activeLocalPlatformPoint);
            Vector3 moveDistance = (newGlobalPlatformPoint - activeGlobalPlatformPoint);
			if (activePlatform.GetComponent<FallBlock>())
				moveDistance.y = 0;
            if (moveDistance != Vector3.zero)
                controller.Move(moveDistance);
            lastPlatformVelocity = (newGlobalPlatformPoint - activeGlobalPlatformPoint) / Time.deltaTime;
            if (activePlatform.GetComponent<GenericBlock>().ReachedDestination())
                activePlatform = null;
        }
        else {
            lastPlatformVelocity = Vector3.zero;
        }
        if (!hasJumped)
            activePlatform = null;
    }
    void GetPlatform()
    {
        if (activePlatform != null) {
            activeGlobalPlatformPoint = transform.position;
            activeLocalPlatformPoint = activePlatform.InverseTransformPoint(transform.position);
        }
    }

    private float ReturnClampedValues(float currentValue, float proposedValue)
    {
        return Mathf.Abs(currentValue) < Mathf.Abs(proposedValue) ? currentValue : proposedValue;
    }
    void OnControllerColliderHit(ControllerColliderHit hit) {
        // Make sure we are really standing on a straight platform
        // Not on the underside of one and not falling down from it either!
        if (hit.moveDirection.y < -0.9 && hit.normal.y > 0.5 && hit.collider.CompareTag("InteractionBlock")) {
            activePlatform = hit.collider.transform;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Fragment")) {
            other.GetComponent<RepairScript>().DoTask();
        }
        if (other.CompareTag("SafepointBlock")) {
            safeBlockPosition = other.transform.position;
			fallSpeed = 0;
        }

        if (other.CompareTag("Puzzle")) {
            if (!currentPuzzleBlock.Equals(other.gameObject)) {
                other.GetComponent<PuzzleBlockChange>().ChangeColour();
                currentPuzzleBlock = other.gameObject;
            }
        }

        if (other.CompareTag("BreakBlock")) {
            print(fallSpeed);
            if (fallSpeed < -gravity / 2) {
                print("triggered");
                other.gameObject.SetActive(false);
            }
        }
        //print ("true");
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("WrongExit") || other.CompareTag("CorrectExit")) {
            transitionTime -= Time.deltaTime;

            if (transitionTime <= 0.0f) {
                InitiateTransition.Fade(scene, Color.black, 0.5f);
                transitionTime = 2f;
            }
        }
        if (other.CompareTag("MainCamera")) {
            if (other.GetComponent<MouseAimCamera>().ReturnDistance() == 0) {
                foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>())
                    renderer.enabled = false;
            }
        }

        if (GameObject.Find("Fader")) {
            if (GameObject.Find("Fader").GetComponent<LostWoodsExitBehaviour>().playerTransition) {
                if (other.CompareTag("WrongExit")) {
                    float currentY = transform.position.y;
                    transform.position = new Vector3(safeBlockPosition.x, Mathf.Abs(currentY), safeBlockPosition.z);
                } else if (other.CompareTag("CorrectExit")) {
                    int currentLevel = SceneManager.GetActiveScene().buildIndex;
                    SceneManager.LoadScene(currentLevel + 1);
                    GameObject.Find("Manager").GetComponent<LoadingManager>().OnLevelComplete();
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("MainCamera")) {
            if (other.GetComponent<MouseAimCamera>().maxCameraRadius != 0) {
                foreach (Renderer renderer in gameObject.GetComponentsInChildren<Renderer>())
                    renderer.enabled = true;
            }
        }
    }

    private float SetDirection(float currentValue, string setDirection, float damp)
    {
        float direction = Input.GetAxisRaw(setDirection);
        float smoothDirection = Input.GetAxis(setDirection);
        //print ("Axis " + smoothDirection + " AxisRaw " + direction);
        if (smoothDirection != 0 && direction == 0)
            direction = smoothDirection;
        float temp = currentValue;
        if (ground) {
            decimal determinedValue = (currentValue != 0 || direction != 0) ? (decimal)direction - (decimal)currentValue : 0;
            float determinedDirection = (float)determinedValue;
            if (Mathf.Abs(determinedDirection) <= damp)
                determinedDirection = 0;
            if (direction == 0 && smoothDirection == 0 && determinedDirection == 0)
                temp = 0;
            temp += ReturnSignage(determinedDirection) * damp;
        } else {
            temp = currentValue + direction * damp;
        }
        //print (setDirection + " " + temp + " " + damp);
        return Mathf.Abs(temp) > 1 ? 1 * ReturnSignage(currentValue) : temp;
    }

    private float ReturnSignage(float number)
    {
        return number == 0 ? 0 : Mathf.Abs(number) / number;
    }

    public Vector3 ReturnMoveDirection()
    {
        return transform.InverseTransformDirection( moveDirection);
    }
    public bool IsMidJump()
    {
        return moveDirection.y > -jumpSpeed;
    }

    public bool GetGround()
    {
        return ground;
    }

    public bool getHasJumped()
    {
        return hasJumped;
    }
    public float getjs()
    {
        return jumpSpeed;
    }

    public float getMoveY()
    {
        return moveDirection.y;
    }
}
