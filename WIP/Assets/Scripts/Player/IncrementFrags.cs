﻿using UnityEngine;
using System.Collections;

public class IncrementFrags : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player"))
			other.GetComponent<FragmentTracker> ().IncrementCount ();
	}
}
