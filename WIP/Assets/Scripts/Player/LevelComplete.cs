﻿using UnityEngine;
using System.Collections;

public class LevelComplete : MonoBehaviour {
	public GameObject portal;

	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag ("Player")) {
			portal.SetActive (true);
			other.GetComponent<FragmentTracker> ().IncrementCount ();
			Destroy (gameObject);
		}	
	}
}
