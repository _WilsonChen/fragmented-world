﻿using UnityEngine;
using System.Collections;

public class FragmentTracker : MonoBehaviour {
	public static FragmentTracker instance;

	private static int fragmentcount;

	void Awake() {
		if (instance == null)
			instance = this;
		else
			Destroy (this);
	}

	public int ReturnFragCount()
	{
		return fragmentcount;
	}

	public void IncrementCount()
	{
		fragmentcount++;
	}
}
