﻿using UnityEngine;
using System.Collections;

public class playerAnimator : MonoBehaviour
{

	private bool prevGround;
	private bool jumpRise;

	private bool isAirborne;

	// Use this for initialization
	void Start ()
	{
		isAirborne = false;
		// Set all animations to loop
		GetComponent<Animation> ().wrapMode = WrapMode.Loop;
		// except jumping
		GetComponent<Animation> () ["JumpRise"].wrapMode = WrapMode.ClampForever;
		GetComponent<Animation> () ["JumpFall"].wrapMode = WrapMode.Once;

		// Put idle and walk into lower layers (The default layer is always 0)
		// This will do two things
		// - Since shoot and idle/walk are in different layers they will not affect
		// each other's playback when calling CrossFade.
		// - Since shoot is in a higher layer, the animation will replace idle/walk
		// animations when faded in.
		GetComponent<Animation> () ["JumpRise"].layer = 1;
		GetComponent<Animation> () ["JumpFall"].layer = 1;

		// Stop animations that are already playing
		//(In case user forgot to disable play automatically)
		GetComponent<Animation> ().Stop ();

		GetComponent<Animation> () ["Run"].weight = 0.8f;
		GetComponent<Animation> () ["JumpRise"].weight = 1;
		GetComponent<Animation> () ["JumpFall"].weight = 1;
		GetComponent<Animation> () ["Idle"].weight = 0.2f;
		GetComponent<Animation> () ["RunLeft"].weight = 0.8f;
		GetComponent<Animation> () ["RunRight"].weight = 0.8f;

		GetComponent<Animation> () ["Run"].speed = 2;
		GetComponent<Animation> () ["JumpRise"].speed = 200;
		GetComponent<Animation> () ["JumpFall"].speed = 200;
		GetComponent<Animation> () ["Idle"].speed = 1;
		GetComponent<Animation> () ["RunLeft"].speed = 2;
		GetComponent<Animation> () ["RunRight"].speed = 2;
	}

	// Update is called once per frame
	void Update ()
	{
		// Based on the key that is pressed,
		// play the walk animation or the idle animation
		CharController character = transform.parent.gameObject.GetComponent<CharController> ();
		if (character.GetGround () && isAirborne) {
			GetComponent<Animation> ().CrossFade ("JumpFall");
			isAirborne = false;
		} else if (!character.GetGround () && !isAirborne) {
			GetComponent<Animation> ().CrossFade ("JumpRise");
			isAirborne = true;
		}

		Vector3 direction = character.ReturnMoveDirection ();
		//print (direction);
        if (direction.z != 0) {
            GetComponent<Animation>().CrossFade("Run");
            if (direction.z > 0) {
                if (direction.x < -5)
                    GetComponent<Animation>().CrossFade("RunLeft");
                else if (direction.x > 5)
                    GetComponent<Animation>().CrossFade("RunRight");
            }
            else {
                if (direction.x < -5)
                    GetComponent<Animation>().CrossFade("RunRight");
                else if (direction.x > 5)
                    GetComponent<Animation>().CrossFade("RunLeft");
            }
		} else if (direction.x != 0) {
			if (direction.x < -0.1)
				GetComponent<Animation> ().CrossFade ("RunLeft");
			else if (direction.x > 0.1)
				GetComponent<Animation> ().CrossFade ("RunRight");
		} else
			GetComponent<Animation> ().CrossFade ("Idle");

		//Land
//        if (jumpRise && !character.getHasJumped() && character.getGround())
//        {
//            GetComponent<Animation>().CrossFade("JumpFall");
//            Debug.Log("fuck");
//            Debug.Log("jumpspeed: " + character.getjs());
//            jumpRise = false;
//        }
//
//        // Jump
//        //Debug.Log(transform.parent.gameObject.GetComponent<CharController>().getMoveDirection());
//        if (Input.GetKeyDown(KeyCode.Space) && character.getGround())
//        {
//            GetComponent<Animation>().CrossFade("JumpRise");
//            jumpRise = true;
//        }
//        prevGround = character.getHasJumped();

		//Debug.Log("jumpspeed: " + transform.parent.gameObject.GetComponent<CharController>().getjs());
		// Debug.Log("moveDirY: " + transform.parent.gameObject.GetComponent<CharController>().getMoveY());
		//Debug.Log(prevGround);
	}
}
