﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour {
	public float speed;
	public float airDamp;

	private bool isGrounded;
	private float currentDamp;

	// Use this for initialization
	void Start () {
		isGrounded = false;
		currentDamp = 1;
	}
	
	// Update is called once per frame
	void Update () {
		print (gameObject.GetComponent<Rigidbody> ().velocity.y.ToString());
		float horizontalMovement = Input.GetAxis ("Horizontal") * Time.deltaTime * speed * currentDamp;
		float verticalMovement = Input.GetAxis ("Vertical") * Time.deltaTime * speed * currentDamp;
		if (Input.GetButtonDown ("Jump") && isGrounded && CheckFall()) {
			gameObject.GetComponent<Rigidbody> ().velocity += new Vector3 (0, 10, 0); 
			currentDamp = airDamp;
			isGrounded = false;
		}
		Vector3 futurePos = new Vector3(0, gameObject.GetComponent<Rigidbody> ().velocity.y, 0);
		futurePos += transform.forward * verticalMovement;
		futurePos += transform.right * horizontalMovement;
		gameObject.GetComponent<Rigidbody> ().velocity = futurePos;
	}

	void OnTriggerEnter(Collider other)
	{
		isGrounded = true;
		currentDamp = 1;
	}

	private bool CheckFall()
	{
		float temp = gameObject.GetComponent<Rigidbody> ().velocity.y;
		return temp < 1E-5 && temp > -1E-5;
	}
}
