﻿using UnityEngine;
using System.Collections;

public class FallBlock : GenericBlock {
	public float breakLimit;

	private Vector3 originalPosition;

	private float vol;
	// Use this for initialization
	public override void Initialise (Transform attachedTransform) {
		base.Initialise(attachedTransform);
		if (breakLimit == 0)
			breakLimit = 1;
		originalPosition = attachedTransform.position;
		ResetCounter ();
		resetPos = false;
	}
	
	// Update is called once per frame
	public override void PerformAction () {
		if (resetPos) {
			AddCount ();
		}

		if (movingCount > breakLimit) {	
			finished = false;
			vol += Time.deltaTime;
			attachedTransform.Translate (attachedTransform.up * -vol);
			resetPos = false;
		} else if (attachedTransform.parent) {
			if (!attachedTransform.parent.GetComponent<ScriptStorage>())
				originalPosition = attachedTransform.position;
		}
	}

	public override void ResetPlatform ()
	{
		attachedTransform.position = originalPosition;
		ResetCounter ();
		resetPos = false;
		vol = 0;
		finished = true;
	}

	public void TriggerPlatform()
	{
		movingCount = breakLimit;
		resetPos = true;
	}
	void OnTriggerEnter(Collider other)
	{
		if(other.CompareTag("Player"))
			resetPos = true;
	}
}
