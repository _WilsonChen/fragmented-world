﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class PlatformResetScript : MonoBehaviour {
	public GameObject placedBlockList;
	[SerializeField]
	private List<ScriptStorage> blockList;

	private bool allowTrigger;
	// Use this for initialization
	void Start () {
		blockList = new List<ScriptStorage> ();
		foreach (ScriptStorage currentBlock in placedBlockList.GetComponentsInChildren<ScriptStorage>()) {
			blockList.Add (currentBlock);
		}
	}
		
	void OnTriggerEnter(Collider other)
	{
		if (enabled) {
			foreach (ScriptStorage currentBlock in blockList) {
				currentBlock.ResetAllBlocks ();
			}
		}
	}
}
