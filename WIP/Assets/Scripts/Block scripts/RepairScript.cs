﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RepairScript : MonoBehaviour {
	public GameObject placedBlockList;
	public bool isLocked;
	protected List<ScatterBlock> blockList;
	private bool resetAll;
	private int index;
	private bool vacated;
	// Use this for initialization
	void Awake () {
		Initialise ();
	}

	void Update()
	{
		if (resetAll) {
			bool performReset = true;
			foreach (ScatterBlock currentBlock in blockList) {
				if (!currentBlock.completedRepair)
					performReset = false;
			}
			if (performReset) {
				foreach (ScatterBlock currentBlock in blockList) {					
					currentBlock.SetFinish ();
				}
				resetAll = false;
				vacated = false;
				//print (gameObject.name + " is reseting blocks");
			}
		}
		bool currentStatus = true;
		foreach (ScatterBlock currentBlock in blockList) {
			if (currentBlock.MatchIndex (index) || !currentBlock.CanRepair())
				currentStatus = false;
		}
		if (currentStatus)
			vacated = true;
	}

	protected void Initialise()
	{
		index = 0;
		if (placedBlockList == null) {
			if (transform.childCount <= 0)
				placedBlockList = transform.parent.gameObject;
			else
				placedBlockList = gameObject;
		}
		blockList = new List<ScatterBlock> ();
		foreach (ScatterBlock currentBlock in placedBlockList.GetComponentsInChildren<ScatterBlock>()) {
			if (transform.parent != null) {
				if (transform.GetComponent<ReleaseScatterBlockScript> ())
					currentBlock.transform.parent = transform.parent;
			}
			blockList.Add (currentBlock);
		}
		resetAll = false;
		vacated = !isLocked;
	}
	public virtual void DoTask()
	{
		if (vacated) {
			foreach (ScatterBlock currentBlock in blockList) {
				currentBlock.RepairBlock (index);
			}
			resetAll = true;
			vacated = false;
		}
	}

	public bool IsVacated()
	{
		return vacated;
	}
	public void SetBlockList(List<ScatterBlock> setList)
	{
		blockList = setList;
	}

	public void ToggleBlockVisibility(bool state)
	{
		foreach (Transform trans in transform) {
			if (!gameObject.Equals (trans.gameObject))
				trans.GetComponent<Renderer> ().enabled = state;
		}
	}
	public void SetIndex(int index)
	{
		this.index = index;
	}

	public void SetIndicatorBlocks(bool settings)
	{
		foreach (Transform trans in transform) {
			if (!gameObject.Equals(trans.gameObject))
				trans.gameObject.SetActive (settings);
		}
	}

	public List<ScatterBlock> ReturnList()
	{
		return new List<ScatterBlock> (blockList.ToArray ());
	}
}
