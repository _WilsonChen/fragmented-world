﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Base class from which all block related scripts should inherit from.
/// </summary>
public abstract class GenericBlock : MonoBehaviour
{
	public float leeWay;
	public float speed;

	protected Transform attachedTransform { get; set; }
	protected List<Vector3> posList = new List<Vector3>();
	[SerializeField]
	protected float movingCount;
	protected bool resetPos;
	[SerializeField]
	protected bool finished;

	public abstract void PerformAction ();
	public abstract void ResetPlatform ();
	public bool IsFinished()
	{
		return finished;
	}

	public virtual void Initialise(Transform attachedTransform)
	{
		finished = true;
		this.attachedTransform = attachedTransform;
		if (speed == 0)
			speed = 0.25f;
		ResetCounter ();
	}

	protected void LerpBlocks(Vector3 a, Vector3 b)
	{
		attachedTransform.position = Vector3.Lerp (a, b, movingCount);
		AddCount ();
	}

	protected void ExpLerpBlocks(Vector3 a, Vector3 b)
	{
		attachedTransform.position = Vector3.Lerp (a, b, speed * Time.deltaTime);
	}

	protected bool ExpLerpBlocks(Vector3 a, Vector3 b, float limit)
	{
		ExpLerpBlocks (a, b);
		return (b - a).magnitude < limit;
	}
	protected void AddCount()
	{
		movingCount += speed * Time.deltaTime;
	}
	protected void ResetCounter()
	{
		movingCount = 0 - leeWay;
	}

	public bool ReachedDestination()
	{
		return movingCount >= 1;
	}

	public Vector3[] ReturnList()
	{
		return posList.ToArray ();
	}
		
}

