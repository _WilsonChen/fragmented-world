﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScriptStorage: MonoBehaviour
{
	private List<GenericBlock> blockBehaviourList;

	[SerializeField]
	private GenericBlock currentScript;
	// Use this for initialization
	void Awake ()
	{
		blockBehaviourList = new List<GenericBlock> ();
		blockBehaviourList.AddRange (GetComponents<GenericBlock> ());
		foreach (GenericBlock script in blockBehaviourList) {
			script.Initialise (transform);
			SetScript (script);
		}
	}
	// Update is called once per frame
	void Update ()
	{
		bool finished = true;
		if (currentScript != null)
			finished = currentScript.IsFinished ();
		if (finished) {
			currentScript = null;
			foreach (GenericBlock script in blockBehaviourList) {
				script.PerformAction ();
				SetScript (script);
			}
		} else
			currentScript.PerformAction ();
	}

	public void ResetAllBlocks ()
	{
		foreach (GenericBlock script in blockBehaviourList) {
			script.ResetPlatform ();
		}
	}

	private void SetScript (GenericBlock script)
	{
		if (!script.IsFinished ())
			currentScript = script;
	}
		
}
