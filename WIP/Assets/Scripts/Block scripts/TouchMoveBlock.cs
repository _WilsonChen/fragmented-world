﻿using UnityEngine;
using System.Collections;

public class TouchMoveBlock : MovingBlock {
	public float resetLimit;

	private bool canMove;
	private bool moving;
	[SerializeField]
	private float resetPosCounter;
	public override void Initialise (Transform attachedTransform)
	{
		canMove = false;
		moving = false;
		resetPosCounter = 0;
		if (resetLimit == 0)
			resetLimit = 2;
		base.Initialise (attachedTransform);
	}
	public override void PerformAction ()
	{
		if (canMove) {
			resetPosCounter = 0;
			moving = true;
			canMove = false;
		}
		else
			resetPosCounter += Time.deltaTime;
		if (moving) {
			MoveBlocksThroughList (false);
		}
		if (resetPosCounter > resetLimit)
			ResetPlatform ();
		CheckCycle ();
	}
	public override void ResetPlatform ()
	{		
		base.ResetPlatform ();
		moving = false;
		canMove = false;
		resetPosCounter = resetLimit;
	}


	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag ("Player"))
			canMove = true;
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player"))
			canMove = false;
	}
}
