﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MovingBlock : GenericBlock {
	public bool cycle = true;
	public bool finishedCycle { get; protected set; }

	private intVector2 toFromBlocks;

	// Use this for initialization
	public override void Initialise (Transform attachedTransform) {	
		finishedCycle = true;
		ResetLerpCoordinates ();
		base.Initialise (attachedTransform);
		ImplementList ();
		if (cycle && posList.Count > 2)
			GenerateBlockPositionCycle ();
	}

	public override void ResetPlatform()
	{
		ResetLerpCoordinates ();
		ResetCounter ();
		ReturnToStartPos ();
	}
	public override void PerformAction () {
		MoveBlocksThroughList (cycle);
		CheckCycle ();
	}
	protected void ImplementList ()
	{
		posList.Add (attachedTransform.position);
		ReadPlacedBlocks ();
	}

	protected void CheckCycle()
	{
		finishedCycle = false;
		if (movingCount <= 0 && (toFromBlocks == new intVector2 (0, 1)))
			finishedCycle = true;
	}
		
	protected void GenerateBlockPositionCycle()
	{
		List<Vector3> tempList = new List<Vector3> ();
		for (int i = posList.Count - 1; i > 0; i--)
			tempList.Add (posList [i]);		
		posList.AddRange (tempList);
	}
	/// <summary>
	/// Moves the lerp coordinates along the list.
	/// </summary>
	protected void CycleCoordinates()
	{
		int counter = posList.Count - 1;
		toFromBlocks = new intVector2 (toFromBlocks.x + 1 > counter ? 0 : toFromBlocks.x + 1, toFromBlocks.y + 1 > counter ? 0 : toFromBlocks.y + 1);
		ResetCounter ();
	}
	protected void MoveBlocksThroughList(bool cycle)
	{
		bool restartCycle = (toFromBlocks.y < posList.Count - 1 || cycle);
		if (movingCount > 1 + leeWay && restartCycle)
			CycleCoordinates ();
		LerpBlocks (posList[toFromBlocks.x], posList[toFromBlocks.y]);
	}
	/// <summary>
	/// Resets the start and destination points to the origin and subsequent position.
	/// </summary>
	protected void ResetLerpCoordinates()
	{
		toFromBlocks = new intVector2 (0, 1);
	}

	protected void ReturnToStartPos()
	{
		if (posList.Count != 0)
			attachedTransform.position = posList [0];
	}

	protected bool HasBlockCompletedCycle()
	{
		return toFromBlocks.y == posList.Count - 1;
	}
	private void ReadPlacedBlocks()
	{
		foreach (Transform pointer in attachedTransform.GetComponentsInChildren<Transform> ()) {
			if (pointer.tag == "Marker") {
				posList.Add (pointer.position);
				Destroy (pointer.gameObject);
			}
		}
	}
		
}
