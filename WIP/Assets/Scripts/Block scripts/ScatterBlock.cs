﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/// <summary>
/// Scatters and repairs the blocks depending on whether resetPos is true or false.
/// </summary>
/// <param name="range">Range.</param>
public class ScatterBlock : GenericBlock {
	public bool completedRepair { get; private set; }
	public bool startRepaired;
	public float repairLimit;
	public float scatterDistance;

	private Vector3 scatterLocation;

	private bool setPos;
	private bool lockBlock;
	private int currentIndex;

	public override void Initialise (Transform attachedTransform)
	{
		//Init variabled
		if (speed <= 0)
			speed = 10f;
		if (scatterDistance <= 0)
			scatterDistance = 50;
		if (repairLimit <= 0)
			repairLimit = 0.5f;
		base.Initialise (attachedTransform);

		//Checks whether or not to start scattered
		resetPos = !startRepaired;
		lockBlock = startRepaired;
		posList.Add(attachedTransform.position);

		currentIndex = 0;
		finished = startRepaired;
		setPos = true;

		if (resetPos)
			SetScatterLocation (scatterDistance);
	}
	public override void PerformAction () {
		if (!lockBlock)
			ScatterRepairBlocks (scatterDistance);
	}
	public override void ResetPlatform ()
	{
	}
	protected void ScatterRepairBlocks(float range)
	{
		if (resetPos) {
			if (setPos) {
				if (movingCount == 0) {
					SetScatterLocation (range);
					//print ("aye");
				}
				setPos = false;
				finished = false;
				SetComponents (false);
				ResetCounter ();
			} else {
				if (!ReachedDestination())
					ExpLerpBlocks (attachedTransform.position, scatterLocation);
			}
		} else {
			if (!setPos) {
				setPos = true;
				//print (gameObject.name + " has begun reset");
				ResetCounter ();
			} else {
				if (!finished)
					completedRepair = ExpLerpBlocks (attachedTransform.position, posList[currentIndex], repairLimit);
			}
		}

	}
	/// <summary>
	/// Scatters the blocks. Should be used only at the start.
	/// </summary>
	/// <param name="range">Range.</param>
	protected void ScatterBlocks(float range)
	{
		SetScatterLocation (range);
		attachedTransform.position = scatterLocation;
		scatterLocation = attachedTransform.position;
		movingCount = 1;
		setPos = false;
	}
		
	public void AddBlock(Transform givenTransform)
	{
		if (givenTransform.tag == "GlowMarker") 
			posList.Add (givenTransform.position);
	}

	public void AddBlocks(List<Transform> givenTransforms)
	{
		foreach (Transform trans in givenTransforms)
			AddBlock (trans);
	}

	private void SetScatterLocation(float range)
	{
		scatterLocation = attachedTransform.position;
		scatterLocation += attachedTransform.TransformDirection (Vector3.up) * ReturnRange (range);
		scatterLocation += attachedTransform.TransformDirection(Vector3.right)  * ReturnRange (range);
	}

	private float ReturnRange(float range)
	{
		float temp = Random.Range (0, range);
		if (temp < range / 2)
			temp -= range;
		return temp;
	}
	private void SetComponents(bool state)
	{
		foreach (MonoBehaviour scripts in gameObject.GetComponentsInChildren<MonoBehaviour>()) {			
			foreach (Collider col in scripts.GetComponents<Collider>())
				col.enabled = state;
			if (!scripts.gameObject.Equals(gameObject))
				scripts.enabled = state;
		}
	}
	public void RepairBlock(int inputIndex)
	{
		if (inputIndex < posList.Count) {
			currentIndex = inputIndex;
			resetPos = false;
			setPos = false;
			finished = false;
			completedRepair = false;
		}
		else
			print ("Not a valid position");
	}

	public void SetFinish()
	{
		finished = true;
		SetComponents (true);
	}

	public void ReleaseBlock(bool isRepaired)
	{
		resetPos = isRepaired;
		setPos = isRepaired;
		lockBlock = false;
	}

	public bool MatchIndex(int i)
	{
		return currentIndex == i;
	}

	public bool CanRepair()
	{
		return lockBlock == false;
	}
}
