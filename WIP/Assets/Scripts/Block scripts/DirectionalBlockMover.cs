﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class DirectionalBlockMover : GenericBlock {
	public float limit;

	[SerializeField]
	private List<MovingBlock> movingBlockList;
	[SerializeField]
	private MovingBlock currentBlock;
	private int currentPos;

	public override void Initialise (Transform attachedTransform)
	{
		movingBlockList = new List<MovingBlock> ();
		currentPos = 0;
		foreach (MovingBlock block in attachedTransform.GetComponentsInChildren<MovingBlock>()) {
			movingBlockList.Add (block);
		}
		currentBlock = movingBlockList [currentPos];
		DeactivateInactiveBlocks ();
		base.Initialise (attachedTransform);
	}
	public override void ResetPlatform ()
	{
		throw new System.NotImplementedException ();
	}
	public override void PerformAction ()
	{
		AddCount ();
		if (movingCount > limit && currentBlock.finishedCycle) {
			currentPos = currentPos >= movingBlockList.Count - 1 ? 0 : currentPos + 1;
			currentBlock = movingBlockList [currentPos];
			ResetCounter ();
			DeactivateInactiveBlocks ();
		}

		currentBlock.PerformAction ();
	}

	private void DeactivateInactiveBlocks()
	{
		foreach (MovingBlock block in movingBlockList) {
			if (!block.Equals (currentBlock)) {
				block.ResetPlatform ();
				block.gameObject.SetActive (false);
			}
			else
				block.gameObject.SetActive (true);
		}
	}
}
