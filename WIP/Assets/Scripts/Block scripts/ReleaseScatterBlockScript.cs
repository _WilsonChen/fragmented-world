﻿using UnityEngine;
using System.Collections;

public class ReleaseScatterBlockScript : RepairScript {

	void Awake()
	{
		Initialise ();
		foreach (ScatterBlock currentBlock in blockList) {
			if (transform.parent != null) {
					currentBlock.transform.parent = transform.parent;
			}
		}
	}

	public override void DoTask ()
	{
		foreach (ScatterBlock currentBlock in blockList) {
			currentBlock.GetComponent<ScriptStorage> ().ResetAllBlocks ();
			currentBlock.ReleaseBlock (true);
		}
		Destroy (gameObject);
	}
}
