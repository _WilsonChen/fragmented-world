﻿using UnityEngine;
using System.Collections;

public class RepairTrigger : MonoBehaviour {
	public RepairScript script;

	public bool destroyOnUse;

	private bool allowTick;
	private bool completedTask;

	private PlatformResetScript reset;
	private GameObject canvas;
	// Use this for initialization
	void Start () {
		allowTick = false;
		completedTask = false;
		reset = GetComponent<PlatformResetScript> ();
		canvas = Instantiate (Resources.Load ("RestoreCanvas", typeof(GameObject))) as GameObject;
		if (reset)
			reset.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		script.ToggleBlockVisibility (allowTick);
		canvas.SetActive (allowTick);
		if (Input.GetMouseButton (0) && allowTick && script.IsVacated ()) {
			if (!completedTask) {
				completedTask = true;
				if (reset)
					reset.enabled = true;
			}
			allowTick = false;
			if (script != null)
				script.DoTask ();
			if (destroyOnUse)
				Destroy (gameObject);
		}
	}
	void OnTriggerStay(Collider other)
	{
		if (other.CompareTag ("Player")) {
			allowTick = script.IsVacated();
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.CompareTag ("Player"))
			allowTick = false;
	}
}
