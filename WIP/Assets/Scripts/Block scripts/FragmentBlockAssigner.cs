﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FragmentBlockAssigner : MonoBehaviour {
	[SerializeField]
	List<RepairScript> fragmentList;
	[SerializeField]
	List<ScatterBlock> scatterblockList;
	// Use this for initialization
	void Start () {
		fragmentList = new List<RepairScript> ();
		scatterblockList = new List<ScatterBlock> ();
		fragmentList.AddRange (transform.GetComponentsInChildren<RepairScript> ());
		scatterblockList.AddRange (transform.GetComponentsInChildren<ScatterBlock> ());
		int[] index = new int[fragmentList.Count];
		int i = 0;
		foreach (ScatterBlock scatter in scatterblockList) {
			int j = 0;
			foreach (RepairScript repair in fragmentList) {
				int k = 0;
				int l = i;
				foreach (Transform t in repair.transform.GetComponentsInChildren<Transform> ()) {
					k++;
					if (t.CompareTag ("GlowMarker")) {
						//print (l);
						if (l <= 0) {
							scatter.AddBlock (t);
							index [j] = k;
							break;
						} else
							l--;
					}		

				}
				j++;
			}
			i++;
		}
		int counter = 0;
		foreach (RepairScript repair in fragmentList) {
			repair.SetIndex (counter);
			repair.SetBlockList (scatterblockList);
			counter++;
		}
	}
}
