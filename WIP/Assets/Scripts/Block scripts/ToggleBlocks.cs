﻿using UnityEngine;
using System.Collections;

public class ToggleBlocks : GenericBlock {
	public bool reverse;
	public float toggleLimit;
	public Material closeToDecay;

	private bool setState;
	[SerializeField]
	private bool currentState;
	private Material normal;
	[SerializeField]
	private float currentLimit;
	public override void Initialise (Transform attachedTransform)
	{
		base.Initialise(transform);
		normal = attachedTransform.GetComponent<MeshRenderer> ().material;
		if (toggleLimit == 0)
			toggleLimit = 0.6f;
		ResetPlatform ();
	}
	public override void ResetPlatform ()
	{
		ResetCounter ();
		if (reverse) {
			movingCount -= toggleLimit / 4;
		}
		attachedTransform.GetComponent<MeshRenderer> ().material = normal;
		setState = !reverse;
		currentState = setState;
		DetermineToggleDuration ();
		ToggleBlock (currentState);
	}
	public override void PerformAction ()
	{
		MeshRenderer render = GetComponent<MeshRenderer> ();
		if (reverse != setState) {
			ToggleBlock (currentState);
			setState = reverse;
			DetermineToggleDuration ();
		}
		AddCount ();
		if (movingCount > currentLimit / 2)
			render.material = closeToDecay;
		if (movingCount > currentLimit) {
			currentState = !currentState;
			render.material = normal;
			ToggleBlock (currentState);
			ResetCounter ();
			DetermineToggleDuration ();
		}
	}

	private void ToggleBlock(bool setValue)
	{
		attachedTransform.GetComponent<Renderer> ().enabled = setValue;
		foreach (BoxCollider boxC in attachedTransform.GetComponents<BoxCollider>())
			boxC.enabled = setValue;
	}

	private void DetermineToggleDuration()
	{
		currentLimit = currentState ? toggleLimit : toggleLimit / 2;
	}
}
