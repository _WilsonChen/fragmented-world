﻿using UnityEngine;
using System.Collections;

public class CreditsFinish : MonoBehaviour {
	public int index;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButton ("Cancel"))
			LoadMenu ();
	}

	public void LoadMenu()
	{
		LoadingScreenManager.LoadScene (index);
	}
}
